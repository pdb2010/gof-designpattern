package c6_adapter.demo3;


import c6_adapter.demo1.TargetAble;

/*接口的适配器模式*/
public abstract class Wrapper2 implements TargetAble {

    public void method1() {
    }

    public void method2() {
    }
}
