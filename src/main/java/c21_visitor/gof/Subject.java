package c21_visitor.gof;

/**
 */
public interface Subject {

    void accept(Visitor visitor);

    String getSubject();
}
