package c21_visitor.baidu;

public interface IVisitor {
    void visit(ConcreteElement1 el1);

    void visit(ConcreteElement2 el2);
}
