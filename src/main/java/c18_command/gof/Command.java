package c18_command.gof;

/**
 * 命令
 */
public interface Command {
    public void exe();
}
