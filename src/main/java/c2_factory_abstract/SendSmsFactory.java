package c2_factory_abstract;

import base.Sender;
import base.SmsSender;

public class SendSmsFactory implements Provider {
    public Sender produce() {
        return new SmsSender();
    }
}
