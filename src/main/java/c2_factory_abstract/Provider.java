package c2_factory_abstract;

import base.Sender;

/**
 * 工厂接口
 */
public interface Provider {
    Sender produce();
}
