package c2_factory_abstract;

import base.MailSender;
import base.Sender;


public class SendMailFactory implements Provider {
    public Sender produce() {
        return new MailSender();
    }
}
