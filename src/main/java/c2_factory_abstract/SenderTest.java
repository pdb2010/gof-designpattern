package c2_factory_abstract;

import base.Sender;

public class SenderTest {

    public static void main(String[] args) {
        Provider provider = new SendMailFactory();
        Sender sender = provider.produce();
        System.out.println(sender.sender());

        provider = new SendSmsFactory();
        sender = provider.produce();
        System.out.println(sender.sender());
    }

}
