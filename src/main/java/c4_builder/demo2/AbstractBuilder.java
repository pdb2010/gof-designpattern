package c4_builder.demo2;

public abstract class AbstractBuilder {

    public abstract void buildPartA();

    public abstract void buildPartB();

    public abstract Product getBuildResult();
}
