package c8_proxy.dynamic;

/**
 * 代理接口
 */
public interface IProxy {

    void buyTicket();
}