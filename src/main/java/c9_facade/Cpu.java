package c9_facade;

public class Cpu {
    public void startup() {
        System.out.println("Cpu startup!");
    }

    public void shutdown() {
        System.out.println("Cpu shutdown!");
    }
}
