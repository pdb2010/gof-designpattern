package c13_strategy.gof;

/**/
public interface ICalculator {
    int calculate(String exp);
}
