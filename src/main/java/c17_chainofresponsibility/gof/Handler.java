package c17_chainofresponsibility.gof;

public interface Handler {
    void operator();
}
