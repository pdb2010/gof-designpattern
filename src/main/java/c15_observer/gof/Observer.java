package c15_observer.gof;

/**
 * 观察者
 */
public interface Observer {
    void update();
}
