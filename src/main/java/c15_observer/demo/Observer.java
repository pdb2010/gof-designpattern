package c15_observer.demo;

/**
 * 抽象观察者
 */
public abstract class Observer {

    public abstract void update();
}
